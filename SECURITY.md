# Security Policy

## Supported Versions

A table will be listed here with supported versions when there become official releases.

## Reporting a Vulnerability

For minor security vulnerabilities, just open an issue on GitHub.

For major security vulnerabilities please either:
- Email us at daddyandwilliam@gmail.com
- Contact us on Discord at Epsilqn#2315
- DM us on Twitter at @Epsilqn
