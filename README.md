# Sokudo
Sokudo (so-kew-dough) is a programming language for all users and purposes. It's designed to be simple, quick, efficient, portable, and powerful.
